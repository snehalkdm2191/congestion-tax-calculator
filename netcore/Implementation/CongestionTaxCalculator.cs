using congestion.calculator.Interface;
using congestion.calculator.models;
using congestion.calculator.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace congestion.calculator.Implementation
{
    public class CongestionTaxCalculator : ICongestionTaxCalculator
    {
        private readonly List<TaxRules> taxRules;
        private readonly List<MaxTaxLimitRules> maxTaxLimitRules;

        public CongestionTaxCalculator()
        {
            taxRules = "TaxRatesForCities.json".LoadJsonAsList<TaxRules>();
            maxTaxLimitRules = "MaximumTaxRulesForCities.json".LoadJsonAsList<MaxTaxLimitRules>();
        }

        /// <summary>
        /// Returns total tax. 
        /// </summary>
        /// <param name="vehicle">Vehicle type</param>
        /// <param name="dates">Array of dates</param>
        /// <param name="city">Name of city</param>
        /// <returns>Returns 0 if toll free vehicles else run the tax rule calculator for selected city</returns>
        public int GetTax(IVehicle vehicle, DateTime[] dates, string city)
        {
            if (IsTollFreeVehicle(vehicle))
                return 0;

            return GetTotalTax(dates, city);
        }

        /// <summary>
        /// Get total tax based on Tax rules, Toll free days, maximum tax per day and max time in given time limit at different toll places.
        /// </summary>
        /// <param name="dates">Array of dates</param>
        /// <param name="city">Name of City to get rule set</param>
        /// <returns>Total tax after evaluating all the rules</returns>
        private int GetTotalTax(DateTime[] dates, string city)
        {
            // Get tax rules for the city
            var taxRulesForCity = taxRules.Where(x => x.applicableCities.Contains(city, StringComparer.OrdinalIgnoreCase)).FirstOrDefault();

            // get maximum tax amount and max time rules for the city.
            var maxTaxLimitRulesForCity = maxTaxLimitRules.Where(x => x.applicableCities.Contains(city, StringComparer.OrdinalIgnoreCase)).FirstOrDefault();

            // If rules are not updated for the city then return 0 tax amount.
            if (taxRulesForCity == null)
                return 0;
            else
            {
                // Get toll tax for all the events
                var data = dates.ToDictionary(x => x, x => GetTollFee(x, taxRulesForCity));

                // Check if max tax limit rule is applied.
                if (maxTaxLimitRulesForCity != null)
                {
                    if (maxTaxLimitRulesForCity.singleChargeRuleLimitInMinutes.HasValue)
                    {
                        foreach (var item in dates)
                        {
                            if (data[item] > 0)
                            {
                                // Get events in next minutes range - Set in  singleChargeRuleLimitInMinutes settings 
                                var eventsInNextMinutes = data.Where(x => x.Key >= item && x.Key < item.AddMinutes(maxTaxLimitRulesForCity.singleChargeRuleLimitInMinutes.Value));

                                // Find all the events in specific time range (Set in singleChargeRuleLimitInMinutes) and assign maximum toll tax for the event
                                data[item] = eventsInNextMinutes.Select(x => x.Value).Max();

                                // Set 0 toll tax for all the subsequent toll events in specific time range (Set in singleChargeRuleLimitInMinutes) except first event
                                eventsInNextMinutes.Where(x => x.Key != item).ToList().ForEach(x => data[x.Key] = 0);
                            }
                        }
                    }

                    // Check if maxTaxPerDay setting is set
                    if (maxTaxLimitRulesForCity.maxPerDayTaxAmount.HasValue)
                    {
                        // Find total tax for each day and if it exceeds maxPerDayTaxAmount value then assign maxPerDayTaxAmount else sum of total tax for the day.
                        data = data.GroupBy(x => x.Key.Date).ToDictionary(x => x.Key, x => x.Sum(y => y.Value) > maxTaxLimitRulesForCity.maxPerDayTaxAmount.Value ?
                                         maxTaxLimitRulesForCity.maxPerDayTaxAmount.Value : x.Sum(y => y.Value));
                    }
                }
                // Return total tax after analysing all the rules
                return data.Values.Sum();
            }
        }

        /// <summary>
        /// Get tax rate based on start time and end time from tax table (Defined in TaxRatesForCities.JSON)
        /// </summary>
        /// <param name="date">Date</param>
        /// <param name="taxRulesForCity">Tax rule object with start time, end time and rate details for the selected city</param>
        /// <returns>returns Tax rate. 0 for toll free dates.</returns>
        private int GetTollFee(DateTime date, TaxRules taxRulesForCity)
        {
            if (IsTollFreeDate(date))
                return 0;

            // Check if datetime for the toll event is matching with any tax rules
            return taxRulesForCity.timeRules.Where(x =>
                     (date.TimeOfDay >= TimeSpan.Parse(x.startTime) && date.TimeOfDay <= TimeSpan.Parse(x.toTime) && !x.tillNextDay) ||
                     (date.TimeOfDay >= TimeSpan.Parse(x.startTime) && x.tillNextDay)).FirstOrDefault().rate;
        }

        /// <summary>
        /// Toll free date - The tax is not charged on weekends (Saturdays and Sundays), public holidays, days before a public holiday and during the month of July.
        /// </summary>
        /// <param name="date">Date</param>
        /// <returns>true if toll free date else false.</returns>
        private Boolean IsTollFreeDate(DateTime date)
        {
            var year = date.Year;
            var month = date.Month;
            var day = date.Day;

            if (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
                return true;

            if (year == 2013)
            {
                if ((month == 1 && day == 1) ||
                    (month == 3 && (day == 28 || day == 29)) ||
                    (month == 4 && (day == 1 || day == 30)) ||
                    (month == 5 && (day == 1 || day == 8 || day == 9)) ||
                    (month == 6 && (day == 5 || day == 6 || day == 20 || day == 21)) ||
                    (month == 7) ||
                    (month == 11 && day == 1) ||
                    (month == 12 && (day == 24 || day == 25 || day == 26 || day == 31)))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Tax Exempt vehicles
        /// </summary>
        /// <param name="vehicle">Type of vehicle</param>
        /// <returns>true if exempted else false</returns>
        private bool IsTollFreeVehicle(IVehicle vehicle)
        {
            if (vehicle == null)
                return false;
            else
            {
                // Check if vehicle type is not added in toll free list of vehicles enum set.
                return Enum.IsDefined(typeof(TollFreeVehicles), vehicle.GetVehicleType());
            }
        }
    }
}