﻿using System;
using System.Collections.Generic;

namespace congestion.calculator.Interface
{
    public interface ICongestionTaxCalculator
    {
        int GetTax(IVehicle vehicle, DateTime[] dates, string city);
    }
}
