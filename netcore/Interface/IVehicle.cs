using System;

namespace congestion.calculator.Interface
{
    public interface IVehicle
    {
        String GetVehicleType();
    }
}