﻿using congestion.calculator.Interface;
using congestion.calculator.models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace congestion.calculator
{
    public static class Helper
    {
        public static T LoadJson<T>(this string fileName)
        {
            using (StreamReader r = new StreamReader($"TaxRulesData/{fileName}"))
            {
                string json = r.ReadToEnd();
                return JsonConvert.DeserializeObject<T>(json);
            }
        }

        public static List<T> LoadJsonAsList<T>(this string fileName)
        {
            using (StreamReader r = new StreamReader($"TaxRulesData/{fileName}"))
            {
                string json = r.ReadToEnd();
                return JsonConvert.DeserializeObject<List<T>>(json);
            }
        }

        public static IVehicle GetVehicle(this VehicleTypes vehicleType)
        {
            return vehicleType switch
            {
                VehicleTypes.Bus => new Bus(),
                VehicleTypes.Car => new Car(),
                VehicleTypes.Diplomat => new Diplomat(),
                VehicleTypes.Emergency => new Emergency(),
                VehicleTypes.Foreign => new Foreign(),
                VehicleTypes.Military => new Military(),
                VehicleTypes.Motorcycle => new Motorcycle(),
                VehicleTypes.Tractor => new Tractor(),
                _ => throw new NotImplementedException()
            };
        }

        public static DateTime[] ConverToDate(this string[] dates)
        {
            try
            {
                return dates.Select(x => DateTime.ParseExact(x, "yyyy-MM-dd HH:mm:ss", null, System.Globalization.DateTimeStyles.None))
                    .Distinct().OrderBy(x => x).ToArray();
            }
            catch (Exception ex)
            {
                throw new InvalidCastException("Please provide dates in 'yyyy-MM-dd HH:mm:ss' format!");
            }
        }
    }
}
