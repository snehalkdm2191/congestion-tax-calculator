﻿using System;

namespace congestion.calculator.Models.DataModels
{
    internal class TaxRules
    {
        public string[] applicableCities { get; set; }
        public TimeRule[] timeRules { get; set; }

        public class TimeRule
        {
            public string startTime { get; set; }
            public string toTime { get; set; }
            public int rate { get; set; }

            public bool tillNextDay
            {
                get
                {
                    return
                        DateTime.ParseExact(startTime, "HH:mm", null, System.Globalization.DateTimeStyles.None) >
                        DateTime.ParseExact(toTime, "HH:mm", null, System.Globalization.DateTimeStyles.None);
                }
            }
        }
    }
}