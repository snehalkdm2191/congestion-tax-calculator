﻿namespace congestion.calculator.Models.DataModels
{
    internal class MaxTaxLimitRules
    {
        public string[] applicableCities { get; set; }
        public int? maxPerDayTaxAmount { get; set; }
        public int? singleChargeRuleLimitInMinutes { get; set; }
    }
}
