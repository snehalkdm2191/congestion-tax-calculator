using congestion.calculator.Interface;
using System;

namespace congestion.calculator.models
{
    public class Emergency : IVehicle
    {
        public String GetVehicleType()
        {
            return "Emergency";
        }
    }
}