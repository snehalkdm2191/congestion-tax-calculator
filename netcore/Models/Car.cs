using congestion.calculator.Interface;
using System;

namespace congestion.calculator.models
{
    public class Car : IVehicle
    {
        public String GetVehicleType()
        {
            return "Car";
        }
    }
}