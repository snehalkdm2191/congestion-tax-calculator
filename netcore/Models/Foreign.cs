using congestion.calculator.Interface;
using System;

namespace congestion.calculator.models
{
    public class Foreign : IVehicle
    {
        public String GetVehicleType()
        {
            return "Foreign";
        }
    }
}