using congestion.calculator.Interface;
using System;

namespace congestion.calculator.models
{
    public class Diplomat : IVehicle
    {
        public String GetVehicleType()
        {
            return "Diplomat";
        }
    }
}