using congestion.calculator.Interface;
using System;

namespace congestion.calculator.models
{
    public class Bus : IVehicle
    {
        public String GetVehicleType()
        {
            return "Bus";
        }
    }
}