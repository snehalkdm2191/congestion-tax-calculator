using congestion.calculator.Interface;
using System;

namespace congestion.calculator.models
{
    public class Military : IVehicle
    {
        public String GetVehicleType()
        {
            return "Military";
        }
    }
}