using congestion.calculator.Interface;
using System;

namespace congestion.calculator.models
{
    public class Tractor : IVehicle
    {
        public String GetVehicleType()
        {
            return "Tractor";
        }
    }
}