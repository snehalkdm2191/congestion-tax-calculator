using congestion.calculator.Interface;
using System;

namespace congestion.calculator.models
{
    public class Motorcycle : IVehicle
    {
        public String GetVehicleType()
        {
            return "Motorcycle";
        }
    }
}