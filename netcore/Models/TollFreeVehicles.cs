﻿namespace congestion.calculator.models
{
    internal enum TollFreeVehicles
    {
        Motorcycle,
        Emergency,
        Diplomat,
        Foreign,
        Military,
        Bus
    }
}