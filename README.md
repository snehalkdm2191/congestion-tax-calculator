# Congestion Tax Calculator

Welcome the Volvo Cars Congestion Tax Calculator assignment.

This repository contains a developer [assignment](ASSIGNMENT.md) used as a basis for candidate intervew and evaluation.

Clone this repository to get started. Due to a number of reasons, not least privacy, you will be asked to zip your solution and mail it in, instead of submitting a pull-request.

1. Run congestiontax_service API Project.
2. Call "CongestionTax" endpoint from swagger.
	1. Select vehicle type
	2. Currently GothenBurg is supported. Add more cities and rules in "MaximumTaxRulesForCities.json" and "TaxRatesForCities.json" files.
	3. Request body - example -
		[
		"2013-01-14 21:00:00",
		"2013-01-15 21:00:00",
		"2013-02-07 06:23:27",
		"2013-02-07 15:27:00",
		"2013-02-08 06:27:00",
		"2013-02-08 06:20:27",
		"2013-02-08 14:35:00",
		"2013-02-08 15:29:00",
		"2013-02-08 15:47:00",
		"2013-02-08 16:01:00",
		"2013-02-08 16:48:00",
		"2013-02-08 17:49:00",
		"2013-02-08 18:29:00",
		"2013-02-08 18:35:00",
		"2013-03-26 14:25:00",
		"2013-03-28 14:07:27"
		]
