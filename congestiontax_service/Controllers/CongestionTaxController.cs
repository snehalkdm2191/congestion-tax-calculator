﻿using congestion.calculator;
using congestion.calculator.Interface;
using congestion.calculator.models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace congestiontax_service.Controllers
{
    [ApiController]
    public class CongestionTaxController : ControllerBase
    {
        private readonly ICongestionTaxCalculator _congestionTaxCalculator;
        private readonly ILogger _logger;

        public CongestionTaxController(ILogger<CongestionTaxController> logger, ICongestionTaxCalculator congestionTaxCalculator)
        {
            _logger = logger;
            _congestionTaxCalculator = congestionTaxCalculator;
        }

        [HttpPost("CongestionTax")]
        public IActionResult GetInfringement(VehicleTypes vehicleType, [FromBody] string[] dateValues, string city = "Gothenburg")
        {
            try
            {
                var dates = dateValues.ConverToDate();
                var tax = _congestionTaxCalculator.GetTax(vehicleType.GetVehicle(), dates, city);
                return Ok(tax);
            }
            catch (InvalidCastException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception ex)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }       
    }
}
